import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/**
 * Created by Owner on 12/3/2015.
 */
public class AssociationRules {
    ArrayList<HashSet<String>> T = new ArrayList<>();
    HashSet<HashSet<String>> F2 = new HashSet<>();
    HashSet<String> outputs = new HashSet<>();

    public void genRules(ArrayList<HashSet<String>> T, HashSet<HashSet<String>> F, double minConf) {
        this.T = T;
        this.F2 = F;

        HashSet<HashSet<String>> h1 = new HashSet<HashSet<String>>();
        for(HashSet<String> f : F) {
            int k = 2;
            if(f.size() >= k) {
                for(String s : f) { //Foreach s belonging to f
                    HashSet<String> fMinusS = new HashSet<String>();
                    //Copy f to fMinusS but remove S.
                    for(String i : f) {
                        if(i != s) {
                            fMinusS.add(i);
                        }
                    }
                    HashSet<String> sSet = new HashSet<String>();
                    sSet.add(s);

                    if(confidenceAssociation(fMinusS, sSet) >= minConf) {
                        h1.add(sSet);
                    }
                    apGenRules(f, h1, 1, minConf);
                }
            }
        }
        for(String s: outputs){
            System.out.println(s);
        }
    }

    public void apGenRules(HashSet<String> f, HashSet<HashSet<String>> Hm, int m, double minConf) {
        if(Hm != null && F2.size() > m + 1) {
            HashSet<HashSet<String>> newH = candidateGen(Hm, m);
            if(newH != null) {
                for (HashSet<String> h : newH) {
                    HashSet<String> fMinusH = new HashSet<String>();
                    HashSet<String> hSet = new HashSet<>();

                    for(String i : f) {
                        fMinusH.add(i);
                    }
                    fMinusH.removeAll(h);
                    for(String i : f) {
                        if(!fMinusH.contains(i)) {
                            hSet.add(i);
                        }
                    }
                    if(fMinusH.size() != 0 && hSet.size() == 1) {
                        double support = supportAssociation(fMinusH, hSet) * 100;
                        DecimalFormat df = new DecimalFormat("#.####");
                        double confidence = confidenceAssociation(fMinusH, hSet) * 100;
                        if(confidence >= minConf){
                            String output = fMinusH + " ---> " + hSet + " [sup=" + df.format(support) + " conf="
                                    + df.format(confidence) + "]";
                            outputs.add(output);

                        }
                        else{
                            newH.removeAll(hSet);
                        }
                    }
                }
            }
            apGenRules(f, newH, m + 1, minConf);
        }
    }

    public HashSet<String> getUnion(HashSet<String> f1, HashSet<String> f2){
        if(f1 == null)
            return f2;
        HashSet<String> newF1 = new HashSet<>();
        for(String i : f1) {
            newF1.add(i);
        }
        newF1.addAll(f2);
        return newF1;
    }

    public HashSet<HashSet<String>> candidateGen(HashSet<HashSet<String>> F, int k){
        HashSet<HashSet<String>> C = new HashSet<>();
        boolean flag;
        for(HashSet<String> f1: F){
            for(HashSet<String> f2: F){
                if(f1.size() == f2.size() && f1.size() == k){
                    HashSet<String> union = getUnion(f1, f2);
                    if(union.size() == f1.size() + 1){
                        flag = true;
                        HashSet<HashSet<String>> powerset = subsets(union);
                        for(HashSet<String> s : powerset){
                            if(s.size() == union.size()-1){
                                if(!F.contains(s)){
                                    flag = false;
                                }
                            }
                        }
                        if(flag){
                            C.add(union);
                        }
                    }
                }
            }
        }
        if(C.size() == 0) return null;
        return C;
    }

    public HashSet<HashSet<String>> getSetSetUnion(HashSet<HashSet<String>> returnF, HashSet<HashSet<String>> f) {
        for(HashSet<String> set : f) {
            if(!returnF.contains(set)) {
                returnF.add(set);
            }
        }
        return returnF;
    }

    public HashSet<HashSet<String>> subsets(HashSet<String> originalSet){
        HashSet<HashSet<String>> powerSet = new HashSet<>();
        if (originalSet.isEmpty()) {
            powerSet.add(new HashSet<>());
            return powerSet;
        }
        List<String> list = new ArrayList<>(originalSet);
        String head = list.get(0);
        HashSet<String> rest = new HashSet<>(list.subList(1, list.size()));
        for (HashSet<String> set : subsets(rest)) {
            HashSet<String> newSet = new HashSet<>();
            newSet.add(head);
            newSet.addAll(set);
            powerSet.add(newSet);
            powerSet.add(set);
        }
        return powerSet;
    }

    public double supportAssociation(HashSet<String> X, HashSet<String> Y){
        int count = 0;
        HashSet<String> union = new HashSet<>();
        union.addAll(X);
        union.addAll(Y);
        for(HashSet<String> line : T){
            boolean flag = true;
            for(String i: union){
                if(!line.contains(i)){
                    flag = false;
                }
            }
            if(flag){
                count++;
            }
        }
        return ((double)count / T.size());
    }

    public double supportT(HashSet<String> X){
        int count = 0;
        for(HashSet<String> i : T){
            boolean flag = true;
            for(String element: X){
                if(!i.contains(element)){
                    flag = false;
                }
            }
            if(flag){
                count++;
            }
        }
        return ((double)count / T.size());
    }

    public double confidenceAssociation(HashSet<String> X, HashSet<String> Y){
        return (supportAssociation(X, Y) / supportT(X));
    }
}
