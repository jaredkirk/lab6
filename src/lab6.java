import java.io.FileNotFoundException;
import java.util.HashSet;

/**
 * Created by jaredkirk on 12/3/15
 */
public class lab6 {

    public static void main(String[] args) throws FileNotFoundException {

        String file = args[0];
        CreateNewFile newFile = new CreateNewFile();
        newFile.createChickenBowlFile(file);
        //Parser parser = new Parser();
        //parser.parseFile(file);

        FrequentItemsets frequentItemsets = new FrequentItemsets();
        HashSet<HashSet<String>> freqItemSets = frequentItemsets.apriori(newFile.T, newFile.I, .2);
        for(HashSet<String> t : freqItemSets) {
            System.out.println("set: " + t);
        }

        AssociationRules associationRules = new AssociationRules();
        associationRules.genRules(newFile.T, freqItemSets, .64);
    }
}
