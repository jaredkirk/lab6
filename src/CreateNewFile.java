import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;

/**
 * Created by jaredkirk on 12/7/15.
 */
public class CreateNewFile {
    public ArrayList<HashSet<String>> T = new ArrayList<>();
    public HashSet<String> I = new HashSet<>();

    public void createChickenBowlFile(String fileName) throws FileNotFoundException {
        File file = new File(fileName);
        Scanner scanner = new Scanner(file);
        //int maxValue = 0;
        //int k = 0;
        while(scanner.hasNext()) {
            String[] nextLine = scanner.nextLine().replaceAll(" ", "").replaceAll("]", "").replaceAll("\\[", "").split("\t");
            HashSet<String> singleT = new HashSet<>();
            //if (nextLine[2].contains("Tacos")) {
            if(!nextLine[2].contains("NULL")) {
                String orders[] = nextLine[3].split(",");

                for (int i = 0; i < orders.length; i++) {
                    singleT.add(orders[i]);
                    I.add(orders[i]);
                }
                T.add(singleT);
            }
        }
    }
}
