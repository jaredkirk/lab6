import java.util.*;

/**
 * Created by Owner on 12/3/2015.
 */
public class FrequentItemsets {

    int numItems = 0;
    ArrayList<HashSet<String>> T = new ArrayList<>();
    ArrayList<HashSet<HashSet<String>>> Fs = new ArrayList<>();

    public HashSet<HashSet<String>> apriori(ArrayList<HashSet<String>> T, HashSet<String> I, double minSup){
        numItems = T.size();
        this.T = T;
        HashSet<HashSet<String>> F1 = new HashSet<>();
        for(String i : I){
            HashSet<String> element = new HashSet<>();
            element.add(i);
            if(supportT(element) >= minSup){
                F1.add(element);
            }
        }
        int k = 1;
        Fs.add(F1);
        do{
            HashSet<HashSet<String>> C = candidateGen(Fs.get(k-1), k);
            HashMap<HashSet<String>, Integer> count = new HashMap<>();
            if(C != null) {
                for (HashSet<String> c : C) {
                    count.put(c, 0);
                }
                for (HashSet<String> t : T) {
                    for (HashSet<String> c : C) {
                        boolean flag = true;
                        for(String i1 : c) {
                            if(!t.contains(i1)) {
                                flag = false;
                            }
                        }
                        if(flag) {
                            count.put(c, count.get(c) + 1);
                        }
                    }
                }
                HashSet<HashSet<String>> newF = getF(C, count, minSup);
                Fs.add(newF);
                k++;
            } else {
                HashSet<HashSet<String>> newF = null;
                Fs.add(newF);
                k++;
            }
        }while(Fs.get(k - 1) != null);
        HashSet<HashSet<String>> returnF = new HashSet<>();
        for(int i = 1; i < k - 1; i++) {
            returnF = getSetSetUnion(returnF, Fs.get(i));
        }
        HashSet<HashSet<String>> skylineF = getSkylineSets(returnF);
        return skylineF;
    }

    public HashSet<HashSet<String>> getSkylineSets(HashSet<HashSet<String>> returnF){
        HashSet<HashSet<String>> skylineF = new HashSet<>();
        skylineF.addAll(returnF);
        for(HashSet i: returnF) {
            for (HashSet j : returnF) {
                //if the frequent dataset is not a subset of another dataset add it to the new dataset
                if (!i.equals(j) && isSubset(i, j)) {
                    skylineF.remove(i);
                }
            }
        }
        return skylineF;
    }

    public boolean isSubset(HashSet<String> set1, HashSet<String> set2){
        for(String i : set1){
            if(!set2.contains(i)){
                return false;
            }
        }
        return true;
    }

    public HashSet<HashSet<String>> getSetSetUnion(HashSet<HashSet<String>> returnF, HashSet<HashSet<String>> f) {
        for(HashSet<String> set : f) {
            if(!returnF.contains(set)) {
                returnF.add(set);
            }
        }
        return returnF;
    }

    public HashSet<HashSet<String>> getF(HashSet<HashSet<String>> C, HashMap<HashSet<String>, Integer> count, double minSup){
        HashSet<HashSet<String>> newF = new HashSet<>();

        for(HashSet<String> c : C){
            if((double)count.get(c)/numItems >= minSup){
                newF.add(c);
            }
        }
        return newF;
    }

    public HashSet<HashSet<String>> candidateGen(HashSet<HashSet<String>> F, int k){
        HashSet<HashSet<String>> C = new HashSet<>();
        boolean flag;
        for(HashSet<String> f1: F){
            for(HashSet<String> f2: F){
                if(!f1.equals(f2) && f1.size() == f2.size() && f1.size() == k){
                    HashSet<String> union = getUnion(f1, f2);
                    if(union.size() == f1.size() + 1){
                        flag = true;
                        HashSet<HashSet<String>> powerset = subsets(union);
                        for(HashSet<String> s : powerset){
                            if(s.size() == union.size()-1){
                                if(!F.contains(s)){
                                    flag = false;
                                }
                            }
                        }
                        if(flag){
                            C.add(union);
                        }
                    }
                }
            }
        }
        if(C.size() == 0) return null;
        return C;
    }

    public HashSet<HashSet<String>> subsets(HashSet<String> originalSet){
        HashSet<HashSet<String>> powerSet = new HashSet<>();
        if (originalSet.isEmpty()) {
            powerSet.add(new HashSet<>());
            return powerSet;
        }
        List<String> list = new ArrayList<>(originalSet);
        String head = list.get(0);
        HashSet<String> rest = new HashSet<>(list.subList(1, list.size()));
        for (HashSet<String> set : subsets(rest)) {
            HashSet<String> newSet = new HashSet<>();
            newSet.add(head);
            newSet.addAll(set);
            powerSet.add(newSet);
            powerSet.add(set);
        }
        return powerSet;
    }

    public HashSet<String> getUnion(HashSet<String> f1, HashSet<String> f2){
        if(f1 == null)
            return f2;
        HashSet<String> newF1 = new HashSet<>();
        for(String i : f1) {
            newF1.add(i);
        }
        newF1.addAll(f2);
        return newF1;
    }


    public double supportT(HashSet<String> X){
        int count = 0;
        for(HashSet<String> i : T){
            boolean flag = true;
            for(String element: X){
                if(!i.contains(element)){
                    flag = false;
                }
            }
            if(flag){
               count++;
            }
        }
        return ((double)count / T.size());
    }
}
