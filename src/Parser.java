import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;

/**
 * Created by Owner on 12/3/2015.
 */
public class Parser {
    public ArrayList<HashSet<Integer>> T = new ArrayList<>();
    public HashSet<Integer> I = new HashSet<>();

    public void parseFile(String fileName) throws FileNotFoundException {
        File file = new File(fileName);
        Scanner scanner = new Scanner(file);
        int maxValue = 0;
        int k = 0;
        while(scanner.hasNext()) {
            String[] nextLine = scanner.nextLine().replaceAll(" ", "").split(",");
            HashSet<Integer> singleT = new HashSet<>();
            for(int i = 1; i < nextLine.length; i++) {
                singleT.add(Integer.parseInt(nextLine[i]));
                if(Integer.parseInt(nextLine[i]) > maxValue) maxValue = Integer.parseInt(nextLine[i]);
            }
            T.add(singleT);

        }
        for(int i = 0; i <= maxValue; i++) {
            I.add(i);
        }

    }

}
